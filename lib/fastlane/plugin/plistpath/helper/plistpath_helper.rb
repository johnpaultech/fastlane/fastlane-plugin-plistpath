require 'fastlane_core/ui/ui'

module Fastlane
  UI = FastlaneCore::UI unless Fastlane.const_defined?("UI")

  module Helper
    class PlistpathHelper
      # Find the configuration with a name or return nil
      def self.find_configuration(build_configurations, conf_name)
        build_configurations.find { |conf| conf.name == conf_name }
      end

      # Find the plist path given a configuration
      def self.find_plist_path(build_configuration, root)
        build_configuration.build_settings['INFOPLIST_FILE']
                           .gsub('$(SRCROOT)', root)
                           .gsub('$(PROJECT_DIR)', root)
      end

      # Retrieve the Info.plist path from a target_name and a project
      def self.plist_path_xcproj(project_path, target_name, conf_name = 'Release')
        begin
          project = Xcodeproj::Project.open(project_path)
        rescue RuntimeError => e
          UI.user_error!(e.message)
          return
        end

        target = project.native_targets.find { |tg| tg.name == target_name }
        if target.nil?
          UI.user_error!("Could not find target '#{target_name}'")
          return
        end

        build_configuration = find_configuration(target.build_configurations, conf_name)
        if build_configuration.nil?
          UI.user_error!("Could not find configuration '#{conf_name}'")
          return
        end

        root = File.dirname(project.path)

        path = find_plist_path(build_configuration, root)

        if path == '$(inherited)'
          project_conf = find_configuration(project.build_configurations, conf_name)
          path = find_plist_path(project_conf, root)
        end
        File.absolute_path(path, root)
      end

      def self.find_project_path(workspace_path, scheme_name)
        workspace = Xcodeproj::Workspace.new_from_xcworkspace(workspace_path)
        if workspace.schemes.empty?
          UI.user_error!("Workspace at path #{workspace_path} has no schemes. Does it exist?")
          return
        end
        project_path = workspace.schemes[scheme_name]
        if project_path.nil?
          UI.user_error!("Scheme '#{scheme_name}' does not exist")
          return
        end
        project_path
      end

      # Normalize an action name
      def self.to_action_symbol(action)
        if action.kind_of?(String)
          action = action.downcase.to_sym
        end

        unless [:test, :run, :profile, :archive, :analyze].include?(action)
          UI.user_error!("Unsupported action #{action}")
          return
        end

        action
      end

      # Return an `ing` version of the action verb
      def self.to_ing_action(action)
        action = to_action_symbol(action)
        ing_mapping = {
          test: 'testing',
          run: 'running',
          profile: 'profiling',
          archive: 'archiving',
          analyze: 'analyzing'
        }

        ing_mapping[action]
      end

      # Find the first target name in a build_action for a given action
      def self.first_target_name(build_action, action)
        ing_action = to_ing_action(action)
        condition = "build_for_#{ing_action}?"
        # binding.pry
        entry = build_action.entries.find { |e| e.send(condition) }
        if entry.nil?
          UI.user_error!("Could not find build action supporting #{ing_action}")
          return
        end
        entry.buildable_references.first.target_name
      end

      # Returns the configuration name for a given scheme action
      def self.scheme_build_configuration(scheme, action)
        attribute = "#{to_action_symbol(action)}_action"
        scheme.send(attribute).build_configuration
      end

      # Returns the target and conf name for a scheme and action
      def self.scheme_information(project_path, scheme_name, action = :archive)
        scheme_path = File.join(project_path, "xcshareddata/xcschemes/#{scheme_name}.xcscheme")
        unless File.exist?(scheme_path)
          UI.user_error!("Could not find scheme at path #{scheme_path}. Is it shared?")
          return
        end

        scheme = Xcodeproj::XCScheme.new(scheme_path)
        return first_target_name(scheme.build_action, action), scheme_build_configuration(scheme, action)
      end
    end
  end
end
