require 'fastlane/action'
require_relative '../helper/plistpath_helper'

module Fastlane
  module Actions
    module SharedValues
      PLIST_PATH ||= :PLIST_PATH
    end
    class PlistpathAction < Action
      # Return the project path based on params or throws
      def self.get_project_path(params)
        project = params[:project].to_s
        unless project.empty?
          return File.expand_path(project)
        end

        workspace = params[:workspace].to_s
        scheme = params[:scheme].to_s

        if workspace.empty? || scheme.empty?
          UI.user_error!('Either "project" or "workspace and scheme" are required')
          return
        end
        return Helper::PlistpathHelper.find_project_path(workspace, scheme)
      end

      # Return the target and conf name or throws
      def self.get_target_and_conf_name(project_path, params)
        conf_name = params[:conf_name].to_s
        target_name = params[:target_name].to_s

        unless conf_name.empty? || target_name.empty?
          return conf_name, target_name
        end

        scheme = params[:scheme].to_s
        action = params[:build_action].to_s

        if scheme.empty? || action.empty?
          UI.user_error!('Either "conf_name and target_name" or "scheme and action" are required')
          return
        end
        return PlistpathHelper.scheme_information(project_path, scheme, action)
      end

      def self.run(params)
        project_path = get_project_path(params)
        target, conf_name = get_target_and_conf_name(project_path, params)

        plist_path = Helper::PlistpathHelper.plist_path_xcproj(project_path, target, conf_name)

        Actions.lane_context[SharedValues::PLIST_PATH] = plist_path if File.exist?(plist_path)
        UI.success("Found plist at path #{plist_path}")
        plist_path
      end

      def self.description
        'Allow retrieving the plist path from xcodeproj or xcworkspace files for a target'
      end

      def self.authors
        ["gdollardollar"]
      end

      def self.return_value
        'The path of the plist'
      end

      def self.details
        # Optional:
        ""
      end

      def self.output
        [
          ['PLIST_PATH', 'The path of the plist']
        ]
      end

      def self.available_options
        [
          FastlaneCore::ConfigItem.new(key: :workspace,
                                  env_name: "PLIST_WORKSPACE",
                               description: "A path to the xcworspace file",
                                  optional: false,
                                      type: String),
          FastlaneCore::ConfigItem.new(key: :project,
                                  env_name: "PLIST_PROJECT",
                               description: "A path to the xcodeproj file",
                                  optional: false,
                                      type: String),
          FastlaneCore::ConfigItem.new(key: :scheme,
                                  env_name: "PLIST_SCHEME",
                               description: "The scheme",
                                  optional: false,
                                      type: String),
          FastlaneCore::ConfigItem.new(key: :conf_name,
                                  env_name: "PLIST_CONF_NAME",
                               description: "The configuration name",
                                  optional: false,
                                      type: String),
          FastlaneCore::ConfigItem.new(key: :build_action,
                                  env_name: "PLIST_BUILD_ACTION",
                               description: 'The build action. Must be in archive, run, test, analyze, profile',
                                   default: 'archive',
                              verify_block: proc do |value|
                                              Helper::PlistpathHelper.to_action_symbol(value)
                                            end,
                                      type: String)
        ]
      end

      def self.is_supported?(platform)
        [:ios, :mac].include?(platform)
      end
    end
  end
end
