PlistpathAction = Fastlane::Actions::PlistpathAction
PlistpathHelper = Fastlane::Helper::PlistpathHelper

describe PlistpathAction do
  describe '#get_project_path' do
    it 'Returns the project path if present in param' do
      expect(PlistpathHelper).not_to(receive(:find_project_path))

      expect(PlistpathAction.get_project_path({
        project: '/hello'
      })).to eq('/hello')
    end

    it 'Find the project using the scheme and workspace' do
      expect(PlistpathHelper)
        .to(receive(:find_project_path))
        .with('/my_workspace', 'my_scheme')
        .and_return('/hello')

      expect(PlistpathAction.get_project_path({
        workspace: '/my_workspace',
        scheme: 'my_scheme'
      })).to eq('/hello')
    end

    it 'Fails if it is missing scheme' do
      expect(FastlaneCore::UI).to receive(:user_error!)

      PlistpathAction.get_project_path({ workspace: '/my_workspace' })
    end

    it 'Fails if it is missing workspace' do
      expect(FastlaneCore::UI).to receive(:user_error!)

      PlistpathAction.get_project_path({ scheme: '/my_scheme' })
    end
  end

  describe '#get_target_and_conf_name' do
    it 'Returns the values if present in param' do
      expect(PlistpathHelper).not_to(receive(:scheme_information))

      expect(PlistpathAction.get_target_and_conf_name('whatever', {
        conf_name: 'my_conf',
        target_name: 'my_target'
      })).to eq(['my_conf', 'my_target'])
    end

    it 'Calls the helper if scheme or action are provided' do
      expect(PlistpathHelper)
        .to(receive(:scheme_information))
        .with('/my_path', 'my_scheme', 'archive')
        .and_return(['my_conf', 'my_target'])

      expect(PlistpathAction.get_target_and_conf_name('/my_path', {
          scheme: 'my_scheme',
          build_action: 'archive'
        })).to eq(['my_conf', 'my_target'])
    end

    it 'Fails if it is missing scheme' do
      expect(FastlaneCore::UI).to receive(:user_error!)
      expect(PlistpathHelper).not_to(receive(:scheme_information))

      PlistpathAction.get_target_and_conf_name('whatever', { build_action: 'archive' })
    end

    it 'Fails if it is missing build_action' do
      expect(FastlaneCore::UI).to receive(:user_error!)
      expect(PlistpathHelper).not_to(receive(:scheme_information))

      PlistpathAction.get_target_and_conf_name('whatever', { scheme: '/my_scheme' })
    end
  end

  describe '#run' do
    it 'Returns the plist path' do
      params = { hello: 'hello' }
      project_path = '/blabla'

      expect(PlistpathAction)
        .to(receive(:get_project_path))
        .with(params)
        .and_return(project_path)

      expect(PlistpathAction)
        .to(receive(:get_target_and_conf_name))
        .with(project_path, params)
        .and_return(['my_target', 'my_conf'])

      expect(PlistpathHelper)
        .to(receive(:plist_path_xcproj))
        .with(project_path, 'my_target', 'my_conf')
        .and_return('/hello.plist')

      expect(FastlaneCore::UI).to receive(:success)

      expect(PlistpathAction.run(params)).to(eq('/hello.plist'))
    end
  end
end
