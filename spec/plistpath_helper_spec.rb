# frozen_string_literal: true

PlistpathHelper = Fastlane::Helper::PlistpathHelper

proj_root = File.join(File.dirname(__FILE__), 'fixtures/proj')

project_path = File.join(proj_root, 'test.xcodeproj')
workspace_path = File.join(proj_root, 'test.xcworkspace')

describe PlistpathHelper do
  describe 'plist_path_xcproj' do
    expected = File.expand_path(File.join(proj_root, 'test/Info.plist'))

    it 'Handles a relative path' do
      expect(PlistpathHelper.plist_path_xcproj(project_path, 'relative_path')).to eq(expected)
    end

    it 'Handles SRCROOT' do
      expect(PlistpathHelper.plist_path_xcproj(project_path, 'src_root')).to eq(expected)
    end

    it 'Handles PROJECT_DIR' do
      expect(PlistpathHelper.plist_path_xcproj(project_path, 'project_dir')).to eq(expected)
    end

    it 'Handles inherited' do
      expect(PlistpathHelper.plist_path_xcproj(project_path, 'inherited')).to eq(expected)
    end

    it 'Fails when the target does not exist' do
      target_name = 'whatever'
      expect(FastlaneCore::UI).to receive(:user_error!)
        .with("Could not find target '#{target_name}'")

      PlistpathHelper.plist_path_xcproj(project_path, target_name)
    end

    it 'Fails when the project does not exist' do
      expect(FastlaneCore::UI).to receive(:user_error!)

      PlistpathHelper.plist_path_xcproj('hello', 'hello')
    end

    it 'Fails when the configuration does not exist' do
      conf_name = 'whatever'
      expect(FastlaneCore::UI).to receive(:user_error!)
        .with("Could not find configuration '#{conf_name}'")

      PlistpathHelper.plist_path_xcproj(project_path, 'inherited', conf_name)
    end
  end

  describe 'find_project_path' do
    it 'Fails if the workspace does not exist' do
      expect(FastlaneCore::UI).to receive(:user_error!)
        .with('Workspace at path hello has no schemes. Does it exist?')
      PlistpathHelper.find_project_path('hello', 'hello')
    end

    it 'Fails if the scheme does not exist' do
      expect(FastlaneCore::UI).to receive(:user_error!)
        .with("Scheme 'hello' does not exist")
      PlistpathHelper.find_project_path(workspace_path, 'hello')
    end

    it 'Returns the project path' do
      expect(PlistpathHelper.find_project_path(workspace_path, 'inherited')).to eq(project_path)
    end
  end

  describe 'to_action_symbol' do
    it 'Converts from string' do
      expect(PlistpathHelper.to_action_symbol('Archive')).to eq(:archive)
    end

    it 'Accepts symbols' do
      expect(PlistpathHelper.to_action_symbol(:test)).to eq(:test)
    end

    it 'Rejects wrong strings' do
      expect(FastlaneCore::UI).to receive(:user_error!)
      PlistpathHelper.to_action_symbol('Arc')
    end

    it 'Rejects wrong symbols' do
      expect(FastlaneCore::UI).to receive(:user_error!)
      PlistpathHelper.to_action_symbol(:blob)
    end
  end

  describe 'to_ing_action' do
    it 'Maps the action' do
      expect(PlistpathHelper).to receive(:to_action_symbol).with('Archive').and_return(:archive)
      expect(PlistpathHelper.to_ing_action('Archive')).to eq('archiving')
    end
  end

  scheme = Xcodeproj::XCScheme.new(File.join(proj_root, 'test.xcodeproj/xcshareddata/xcschemes/src_root.xcscheme'))

  describe 'first_target_name' do
    build_action = scheme.build_action
    it 'Returns the right target 1' do
      expect(PlistpathHelper).to receive(:to_ing_action).with(:archive).and_return('archiving')
      expect(PlistpathHelper.first_target_name(build_action, :archive)).to eq('inherited')
    end

    it 'Returns the right target 2' do
      expect(PlistpathHelper).to receive(:to_ing_action).with(:test).and_return('testing')
      expect(PlistpathHelper.first_target_name(build_action, :test)).to eq('src_root')
    end

    it 'Fails when no target match' do
      expect(PlistpathHelper).to receive(:to_ing_action).with(:analyze).and_return('analyzing')
      expect(FastlaneCore::UI).to receive(:user_error!)

      PlistpathHelper.first_target_name(build_action, :analyze)
    end
  end

  describe 'scheme_build_configuration' do
    it 'Returns the right build configuration' do
      expect(PlistpathHelper).to receive(:to_action_symbol).with('Archive').and_return(:archive)
      expect(PlistpathHelper.scheme_build_configuration(scheme, 'Archive')).to eq('Release')
    end
  end

  describe 'scheme_information' do
    it 'Fails when the project does not exist' do
      expect(FastlaneCore::UI).to receive(:user_error!)

      PlistpathHelper.scheme_information(project_path, 'hello')
    end

    it 'Returns the target and conf name' do
      expect(PlistpathHelper).to receive(:first_target_name)
        .with(instance_of(Xcodeproj::XCScheme::BuildAction), :archive)
        .and_return('target')

      expect(PlistpathHelper).to receive(:scheme_build_configuration)
        .with(instance_of(Xcodeproj::XCScheme), :archive)
        .and_return('conf')

      values = PlistpathHelper.scheme_information(project_path, 'src_root', :archive)
      expect(values).to eq(['target', 'conf'])
    end
  end
end
